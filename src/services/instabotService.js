import axios from "axios";

const axiosInstance = axios.create({});
const host = "http://localhost:3001";

export default {
  getInstabot(limit = 5, offset = 0, filter = "username", order = "ASC") {
    return axiosInstance.get(
      `${host}/api/instabots?limit=${limit}&offset=${offset}&filter=${filter}&order=${order}`
    );
  },
  patchInstabot(id, ad) {
    return axiosInstance.patch(`${host}/api/instabots/${id}`, { ad });
  },
  getTotalNbFollowers() {
    return axiosInstance.get(`${host}/api/instabots/followers`);
  },
};
