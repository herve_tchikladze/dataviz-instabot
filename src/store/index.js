import { createStore } from "vuex";
import instabotStore from "@/store/instabot/instabot.module";

const store = createStore({
  modules: {
    instabotStore,
  },
});

export default store;
