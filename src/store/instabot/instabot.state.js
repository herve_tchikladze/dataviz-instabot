import { Mode } from "../../models/mode";

export default {
  mode: Mode.INSTAGRAM,
  instabots: [],
  totalFollowers: 0,
  pagination: {
    limit: 5,
    offset: 0,
  },
  filter: "username",
  order: "ASC",
};
