export const types = {
  SET_MODE: "setMode",
  SET_INSTABOTS: "setInstabots",
  SET_TOTAL_FOLLOWERS: "setTotalFollowers",
  SET_OFFSET: "setOffset",
  SET_LIMIT: "setLimit",
  SET_FILTER: "setFilter",
  SET_ORDER: "setOrder",
};

export default {
  [types.SET_MODE](state, mode) {
    state.mode = mode.mode;
  },
  [types.SET_INSTABOTS](state, instabots) {
    state.instabots = instabots.instabots;
  },
  [types.SET_TOTAL_FOLLOWERS](state, totalFollowers) {
    state.totalFollowers = totalFollowers.totalFollowers;
  },
  [types.SET_OFFSET](state, offset) {
    state.pagination.offset = offset.offset;
  },
  [types.SET_LIMIT](state, limit) {
    state.pagination.limit = limit.limit;
  },
  [types.SET_FILTER](state, filter) {
    state.filter = filter.filter;
  },
  [types.SET_ORDER](state, order) {
    state.order = order.order;
  },
};
