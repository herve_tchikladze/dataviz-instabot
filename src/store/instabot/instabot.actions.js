import { types } from "@/store/instabot/instabot.mutations";
import instabotService from "@/services/instabotService";

export default {
  changeMode({ commit }, mode) {
    commit(types.SET_MODE, {
      mode: mode,
    });
  },
  changeOffset({ commit }, offset) {
    commit(types.SET_OFFSET, {
      offset: offset,
    });
  },
  changeLimit({ commit }, limit) {
    commit(types.SET_LIMIT, {
      limit: limit,
    });
  },
  changeFilter({ commit }, filter) {
    commit(types.SET_FILTER, {
      filter: filter,
    });
  },
  changeOrder({ commit }, order) {
    commit(types.SET_ORDER, {
      order: order,
    });
  },
  async fetchInstabot({ commit }, pagination) {
    const result = await instabotService
      .getInstabot(
        pagination.limit,
        pagination.offset,
        pagination.filter,
        pagination.order
      )
      .catch((error) => {
        throw error;
      });
    if (result.data.length > 0) {
      commit(types.SET_INSTABOTS, {
        instabots: result.data,
      });
    }
    // return new Promise((resolve) => {
    //   setTimeout(() => {
    //     resolve(result.data);
    //   }, 2000);
    // });
    return result.data;
  },
  async changeInstabotAd({ commit }, param) {
    await instabotService.patchInstabot(param.id, param.ad).catch((error) => {
      throw error;
    });
  },
  async fetchTotalFollowers({ commit }) {
    const result = await instabotService
      .getTotalNbFollowers()
      .catch((error) => {
        throw error;
      });
    // return new Promise((resolve) => {
    //   setTimeout(() => {
    //     commit(types.SET_TOTAL_FOLLOWERS, {
    //       totalFollowers: result.data.totalFollowers,
    //     });
    //     resolve(result.data.totalFollowers);
    //   }, 2000);
    // });
    commit(types.SET_TOTAL_FOLLOWERS, {
      totalFollowers: result.data.totalFollowers,
    });
    return result.data.totalFollowers;
  },
};
