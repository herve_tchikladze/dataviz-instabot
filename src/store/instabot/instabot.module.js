import state from "@/store/instabot/instabot.state.js";
import actions from "@/store/instabot/instabot.actions.js";
import mutations from "@/store/instabot/instabot.mutations.js";
import getters from "@/store/instabot/instabot.getters.js";

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
