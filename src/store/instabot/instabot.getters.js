export default {
  mode: (state) => state.mode,
  instabots: (state) => state.instabots,
  totalFollowers: (state) => state.totalFollowers,
  offset: (state) => state.pagination.offset,
  limit: (state) => state.pagination.limit,
  filter: (state) => state.filter,
  order: (state) => state.order,
};
