"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Instabot extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Instabot.init(
    {
      keyword: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      hashtags: DataTypes.ARRAY(DataTypes.STRING),
      fullHashtags: DataTypes.ARRAY(DataTypes.STRING),
      bio: DataTypes.STRING,
      imageUrl: DataTypes.STRING,
      description: DataTypes.STRING,
      posted: DataTypes.ARRAY(DataTypes.STRING),
      ad: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Instabot",
    }
  );
  return Instabot;
};
